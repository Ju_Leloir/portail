# NSI-cookies : l'offre 2023

<table><tr>
  <td><img width="300" src="https://gitlab.com/mooc-nsi-snt/portail/-/raw/master/assets/logo-nsi-cookies.png"/></td>
  <td><br/><br/><br/><br/><br/>
Voici venir le programme 2023 de notre initiative <a href="https://mooc-nsi-snt.gitlab.io/portail/6_Communaute.html">NSI cookies</a>, attention nous sommes encore un peu en  travaux sur cette page :) </td></tr>
</table>

## Notre Calendrier

- _Mardi 24 janvier_ , 19h-20h Webinaire_ : à la découverte de [Capytale](https://ent2d.ac-bordeaux.fr/disciplines/nsi/2022/06/28/capytale/) qui est un environnement de programmation Python entièrement en ligne (sans aucune installation, utilisant [Basthon](https://basthon.fr/)) qui permet de travailler avec les élèves.
   - Nous nous connecterons [sur ce lien](https://visio-agents.education.fr/meeting/signin/159585/creator/800/hash/f1bfa1fe0c9724a35ba2cf9ba4f742ab6232db7f).
   - Nous pouvons en discuter sur [cette catégorie](https://mooc-forums.inria.fr/moocnsi/t/nouvelle-categorie-capytale/6358) du forum.

- Jeudi 2 et vendredi 3 février : présentation de l'initiative [NS_cookies](https://mooc-nsi-snt.gitlab.io/portail/6_Communaute.html) lors des journées NSI de l'Académie de Nice.

- _Mardi 8 février et/ou mardi 15 février, 19h-20h Webinaire_ : 
   - soit sur les bases de données, on pourra suivre le module 1.4 du [MOOC NSI sur les fondamentaux](https://mooc-nsi-snt.gitlab.io/portail/3_Les_Fondamentaux/index.html) accès direct [ici](https://lms.fun-mooc.fr/courses/course-v1:inria+41028+session01/courseware/d21b1c9614df48569d7073009beea699/be434df1f8be484785b3161c1752018f/) pour les personnes inscrites,
  - soit sur un thème plus sociétal comme les réseaux sociaux ou les fake news en lien avec les thèmes SNT sur les réseaux sociaux et le web,
  ceci selon les besoins des personnes

- Mercredi 1 mars et/ou mercredi 8 mars, 14h-17h séance présentielle sur la géolocalisation en lien avec le thème4 SNT : rappel du thème, discussions sur les points de contenu ou de didactique à lever, propositions d'activités et expérimentation
  - La séance aura lieu simultanément à
     -  l'[INSPÉ de la Seyne sur Mer]( https://goo.gl/maps/1TdG6igXNwUa2JbN9), 59 All. Émile Pratali, 83500 La Seyne-sur-Mer
     - [Terra Numerica](https://goo.gl/maps/88z3ws6XUUcEfG2CA), 18 Rue Frédéric Mistral, 06560 Valbonne
    - En ligne  [sur ce lien](https://visio-agents.education.fr/meeting/signin/159585/creator/800/hash/f1bfa1fe0c9724a35ba2cf9ba4f742ab6232db7f).



![En travaux](https://gitlab.com/mooc-nsi-snt/portail/-/raw/master/assets/Under-construction.png)

Ref: https://notes.inria.fr/f5RGGHTtQ9iVDVJeaEuFLw
